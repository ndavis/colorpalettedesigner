#pragma once

#include "kcolorcollection.h"

#include <KAboutData>
#include <KLocalizedString>
#include <KHelpMenu>
#include <KColorScheme>
#include <KColorUtils>

#include <QApplication>
#include <QClipboard>
#include <QColorDialog>
#include <QCommandLineParser>
#include <QDialogButtonBox>
#include <QMenu>
#include <QMimeData>
#include <QPushButton>
#include <QDebug>
#include <QtMath>
#include <QSaveFile>
#include <QTextStream>

#include <iostream>
#include <iomanip>
#include <string>
#include <vector>
