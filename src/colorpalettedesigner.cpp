#include "colorpalettedesigner.h"

int main(int argc, char *argv[])
{
    if (argc < 2) {
        qWarning() << "Too many arguments";
        return 1;
    } else if (argc > 2) {
        qWarning() << "Not enough arguments";
        return 1;
    }
//     QApplication app(argc, argv);
    
    QColor originalColor = argv[1];
    qreal originalHue=KColorUtils::hue(originalColor);
    qreal originalChroma=KColorUtils::chroma(originalColor);
    qreal originalYLuma=KColorUtils::luma(originalColor);
    
    qDebug() << "HCY-H" << originalHue << "HSL-H" << originalColor.hslHueF() << "C" << originalChroma << "Y" << originalYLuma << "\n";
    
    QString paletteName = originalColor.name().remove(QLatin1Char('#'));
    
    KColorCollection paletteFile(paletteName);
    paletteFile.setEditable(KColorCollection::Yes);
    paletteFile.setDescription(QLatin1String("Shades of ") + paletteName);
    
    qDebug() << paletteFile.installedCollections();
    
    QColor tempColor1 = originalColor;
    QColor tempColor2 = KColorUtils::hcyColor(originalHue, originalChroma, 0);
    
    
    int count = 0;
    for (qreal y = 0; y < 1; y += 0.0001) {
        tempColor1 = KColorUtils::hcyColor(originalHue, originalChroma, y);
        if (KColorUtils::contrastRatio(tempColor1, tempColor2) >= 1.01) {
            tempColor2 = tempColor1;
            qDebug()    << "H" << 360*KColorUtils::hue(tempColor2) 
//                         << "HSL-H" << 360*tempColor2.hslHueF() 
                        << "C" << 100*KColorUtils::chroma(tempColor2)
                        << "Y" << 100*KColorUtils::luma(tempColor2) << tempColor2.name() << ++count;
            paletteFile.addColor(tempColor2, tempColor2.name());
        }
    }

    paletteFile.save();
//     app.exec();
    return 0;
}
