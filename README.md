# colorpalettedesigner

This is alpha quality software.

Currently, it just installs palettes to `~/.config/inkscape/palettes`.

I want to add a GUI eventually.

## Usage

```
colorpalettedesigner \#232629   #This generates shades based on #232629
```
